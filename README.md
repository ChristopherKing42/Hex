# Hex
This AI (written in Haskell) will play a strong (though not expert) game of Hex.

It utilizes MCTS with hard rollouts. There are two rollout patterns. The first connects a two-bridge if poked. The second creates a defensive wall against the last played piece if possible. It also uses AMAF (all moves as first).