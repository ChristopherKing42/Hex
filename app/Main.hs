module Main where

import Lib
import Text.Read

askFirst = do
    putStrLn "Would you like to (1) go first (2) go second or (3) go first w/ the swap rule?"
    response <- getLine
    case response of
        "1" -> return $ Just True
        "2" -> return $ Just False
        "3" -> return Nothing
        _ -> askFirst

askTime = do
    putStrLn "How many seconds would like to give the AI per turn? (Default: 10.0)"
    response <- getLine
    case response of
        "" -> return 10.0
        s -> case readMaybe s of
            Just n -> return n
            Nothing -> askTime

main = do
    first <- askFirst
    time <- askTime
    case first of
        Just False -> play start{swapped = True} time
        Just True -> do
            print start
            move <- humanMove start{swapped = True} Nothing
            let b = moveAt move start{swapped = True}
            play b time
        Nothing -> do
            print start
            move <- humanMove start{swapped = False} Nothing
            let b = moveAt move start{swapped = False}
            play b time
